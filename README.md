# Dev Server

## Description :
Dev Server with [<%= EJS %>](https://ejs.co/) Embedded JavaScript templating.

---

### DevDependencies :

- Babel
- Babel/plugin-syntax-dynamic-import
- Babel/preset-env
- Babel/babel-loader
- cross-env
- ejs-loader
- webpack
- webpack-cli
- webpack-dev-server
- html-webpack-plugin

## Getting Started

### Install with NPM:

  ```
    npm i --dev
  ```